from datetime import datetime, timedelta
import time
from calendar import Calendar


class Person:
    def __init__(self, name, birthdate):
        self.name = name
        self.birthdate = birthdate

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def setBirthDate(self, birthdate):
        self.birthdate = birthdate

    def getBirthDate(self):
        return self.birthdate


class BirthdayBook:
    def __init__(self, personList={}):
        self.personList = personList

    def addPerson(self, person):
        self.personList[person.getName()] = person

    def _getBirthdayByName(self, name):
        if self.personList.get(name):
            return self.personList.get(name).getBirthDate()
        return None

    def _getPersonByBirthDateMonth(self, month):
        marches = [];
        for p in self.personList.values():
            if p.getBirthDate().month == month:
                marches.append(p)
        return marches

    def printBirthDateByName(self, name):
        d = self._getBirthdayByName(name)
        if d != None:
            print("Name:", name, "BirthDate:", d.day, "/", d.month, "/", d.year)
            return
        print("Person Not Found")
        wink

    def printBirthdayByMonth(self, month):
        marches = self._getPersonByBirthDateMonth(month)
        if marches != []:
            print("Persons born in the month ", month)
            for p in marches:
                print(p.getName())
        else:
            print("Persons born in the month ", month, "not in DB")
            wink

    def _getPersonsBirthdayWithinTheWeek(self, month, day):
        # This function is assuming
        # Monday as the first day of week and sunday as the last day .. wink
        marches = [];
        currentDatetime = datetime.now()
        userDateWeekday = datetime(currentDatetime.year, month, day).weekday()
        # If for the whole week
        # firstWeekdayTimestamp = datetime(currentDatetime.year,month,day) - timedelta(userDateWeekday)).timestamp()
        firstWeekdayTimestamp = datetime(currentDatetime.year, month, day).timestamp()
        lastWeekdayTimestamp = (datetime(currentDatetime.year, month, day) + timedelta(6 - userDateWeekday)).timestamp()
        for p in self.personList.values():
            pBirthDate = p.getBirthDate()
            if pBirthDate.month == month:
                pBirthDate = datetime(currentDatetime.year, pBirthDate.month, pBirthDate.day)
                ptimestamp = pBirthDate.timestamp()
                if ptimestamp >= firstWeekdayTimestamp and ptimestamp <= lastWeekdayTimestamp:
                    marches.append(p)
        return marches

    def printBirthdayWithinTheWeek(self, month, day):
        marches = self._getPersonsBirthdayWithinTheWeek(month, day)
        if marches != []:
            print("Persons born within the week of ", month, day)
            for p in marches:
                print(p.getName())
        else:
            print("Persons born within the week of ", month, day, "not in DB")
            wink
            if __name__ == '__main__':
                person1 = Person("sanju", datetime(1998, 5, 25))
            person2 = Person("vinoth", datetime(1998, 5, 18))
            person3 = Person("dharma", datetime(1998, 5, 23))
            person4 = Person("akshay", datetime(1998, 5, 21))
            person5 = Person("nishanth", datetime(1998, 2, 21))
            person6 = Person("Dhiyanesh", datetime(1998, 5, 5))
            birthdayBook = BirthdayBook()
            birthdayBook.addPerson(person1)
            birthdayBook.addPerson(person2)
            birthdayBook.addPerson(person3)
            birthdayBook.addPerson(person4)
            birthdayBook.addPerson(person5)
            birthdayBook.addPerson(person6)
            birthdayBook.printBirthDateByName("sanju")
            wink
            birthdayBook.printBirthdayByMonth(2)
            birthdayBook.printBirthdayWithinTheWeek(5, 20)